package it.f2informatica.aws.ses.mailer.utils;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;

public class XmlToHtml {

	public XmlToHtml() {
		
	}

	public static void main(String[] args) {
		

	}

	public static void convertXMLToHTML(Source xml, Source xslt) {
		StringWriter sw = new StringWriter();

		try {

			FileWriter fw = new FileWriter("resources/product.html");
			TransformerFactory.newInstance().newTransformer(xslt).transform(xml, new StreamResult(sw));
			fw.write(sw.toString());
			fw.close();

			System.out.println("OK");

		} catch (IOException | TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		}

}}
